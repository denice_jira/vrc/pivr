from udpClient import UpdClient
from constants import *
from logger import Logger
import time 
import uuid
import numpy as np

class Network(Logger):
    def __init__(self,sensorCount):
        super().__init__("Network")
        self.updClient = UpdClient()
        self.connected = False
        self.lastConnectionAttemptMs = 0
        self.sensorCount = sensorCount
        self.sensorStateNotified = np.zeros(sensorCount, dtype=int)
        self.lastSensorInfoPacket = 0

    def isConnected(self):
        return self.connected

    def resetConnection(self):
        self.connected = False

    #PACKET_HEARTBEAT 0
    def sendHeartbeat(self):
        if(not self.connected):
            return
        self.updClient.beginPacket()
        self.updClient.sendPacketType(PACKET_HEARTBEAT)
        self.updClient.sendPacketNumber()
        self.updClient.endPacket()

    #PACKET_ACCEL 4
    def sendAccel(self,vector,sensorId):
        if(not self.connected):
            return
        self.updClient.beginPacket()
        self.updClient.sendPacketType(PACKET_ACCEL)
        self.updClient.sendPacketNumber()
        self.updClient.sendFloat(vector[0])
        self.updClient.sendFloat(vector[1])
        self.updClient.sendFloat(vector[2])
        self.updClient.endPacket()
    
    #PACKET_RAW_CALIBRATION_DATA 6
    def sendRawCalibrationData(self,vector,calibrationType,sensorId):
        if(not self.connected):
            return

        self.updClient.beginPacket()
        self.updClient.sendPacketType(PACKET_RAW_CALIBRATION_DATA)
        self.updClient.sendPacketNumber()
        self.updClient.sendByte(sensorId)
        self.updClient.sendInt(calibrationType)
        self.updClient.sendFloat(vector[0])
        self.updClient.sendFloat(vector[1])
        self.updClient.sendFloat(vector[2])
        self.updClient.endPacket()

    #PACKET_CALIBRATION_FINISHED 7
    def sendCalibrationFinished(self,calibrationType,sensorId):
        if(not self.connected):
            return

        self.updClient.beginPacket()
        self.updClient.sendPacketType(PACKET_CALIBRATION_FINISHED)
        self.updClient.sendPacketNumber()
        self.updClient.sendByte(sensorId)
        self.updClient.sendInt(calibrationType)
        self.updClient.endPacket()


    #PACKET_BATTERY_LEVEL 12
    def sendBatteryLevel(self,batteryVoltage, batteryPercentage):
        if(not self.connected):
            return

        self.updClient.beginPacket()
        self.updClient.sendPacketType(PACKET_BATTERY_LEVEL)
        self.updClient.sendPacketNumber()
        self.updClient.sendFloat(batteryVoltage)
        self.updClient.sendFloat(batteryPercentage)
        self.updClient.endPacket()


    #PACKET_TAP 13
    def sendTap(self,value,sensorId):
        if(not self.connected):
            return

        self.updClient.beginPacket()
        self.updClient.sendPacketType(PACKET_TAP)
        self.updClient.sendPacketNumber()
        self.updClient.sendByte(sensorId)
        self.updClient.sendByte(value)
        self.updClient.endPacket()


    #PACKET_ERROR 14
    def sendError(self,reason,sensorId):
        if(not self.connected):
            return

        self.updClient.beginPacket()
        self.updClient.sendPacketType(PACKET_ERROR)
        self.updClient.sendPacketNumber()
        self.updClient.sendByte(sensorId)
        self.updClient.sendByte(reason)
        self.updClient.endPacket()


    #PACKET_SENSOR_INFO 15
    def sendSensorInfo(self,sensor):
        if(not self.connected):
            return

        self.updClient.beginPacket()
        self.updClient.sendPacketType(PACKET_SENSOR_INFO)
        self.updClient.sendPacketNumber()
        self.updClient.sendByte(sensor['sensorId'])
        self.updClient.sendByte(sensor['sensorState'])
        self.updClient.sendByte(sensor['sensorType'])
        self.updClient.endPacket()


    #PACKET_ROTATION_DATA 17
    def sendRotationData(self,quaternion,dataType,accuracyInfo,sensorId):
        if(not self.connected):
            return

        self.updClient.beginPacket()
        self.updClient.sendPacketType(PACKET_ROTATION_DATA)
        self.updClient.sendPacketNumber()
        self.updClient.sendByte(sensorId)
        self.updClient.sendByte(dataType)
        self.updClient.sendFloat(quaternion["x"])
        self.updClient.sendFloat(quaternion["y"])
        self.updClient.sendFloat(quaternion["z"])
        self.updClient.sendFloat(quaternion["w"])
        self.updClient.sendByte(accuracyInfo)
        self.updClient.endPacket()


    #PACKET_MAGNETOMETER_ACCURACY 18
    def sendMagnetometerAccuracy(self,accuracyInfo,sensorId):
        if(not self.connected):
            return

        self.updClient.beginPacket()
        self.updClient.sendPacketType(PACKET_MAGNETOMETER_ACCURACY)
        self.updClient.sendPacketNumber()
        self.updClient.sendByte(sensorId)
        self.updClient.sendFloat(accuracyInfo)
        self.updClient.endPacket()


    #PACKET_SIGNAL_STRENGTH 19
    def sendSignalStrength(self,signalStrength):
        if(not self.connected):
            return

        self.updClient.beginPacket()
        self.updClient.sendPacketType(PACKET_SIGNAL_STRENGTH)
        self.updClient.sendPacketNumber()
        self.updClient.sendByte(255)
        self.updClient.sendByte(signalStrength)
        self.updClient.endPacket()


    #PACKET_TEMPERATURE 20
    def sendTemperature(self,temperature,sensorId):
        if(not self.connected):
            return

        self.updClient.beginPacket()
        self.updClient.sendPacketType(PACKET_TEMPERATURE)
        self.updClient.sendPacketNumber()
        self.updClient.sendByte(sensorId)
        self.updClient.sendFloat(temperature)
        self.updClient.endPacket()


    def sendInspectionRawIMUData(self,sensorId,rX,rY,rZ,rA,aX,aY,aZ,aA,mX,mY,mZ,mA):
        if(not self.connected):
            return

        self.updClient.beginPacket()

        self.updClient.sendPacketType(PACKET_INSPECTION)
        self.updClient.sendPacketNumber()

        self.updClient.sendByte(PACKET_INSPECTION_PACKETTYPE_RAW_IMU_DATA)

        self.updClient.sendByte(sensorId)
        self.updClient.sendByte(PACKET_INSPECTION_DATATYPE_INT)

        self.updClient.sendInt(rX)
        self.updClient.sendInt(rY)
        self.updClient.sendInt(rZ)
        self.updClient.sendByte(rA)

        self.updClient.sendInt(aX)
        self.updClient.sendInt(aY)
        self.updClient.sendInt(aZ)
        self.updClient.sendByte(aA)

        self.updClient.sendInt(mX)
        self.updClient.sendInt(mY)
        self.updClient.sendInt(mZ)
        self.updClient.sendByte(mA)

        self.updClient.endPacket()


    def sendInspectionRawIMUData(self,sensorId, rX, rY, rZ,rA, aX, aY, aZ,aA, mX, mY, mZ,mA):
        if(not self.connected):
            return

        self.updClient.beginPacket()

        self.updClient.sendPacketType(PACKET_INSPECTION)
        self.updClient.sendPacketNumber()

        self.updClient.sendByte(PACKET_INSPECTION_PACKETTYPE_RAW_IMU_DATA)

        self.updClient.sendByte(sensorId)
        self.updClient.sendByte(PACKET_INSPECTION_DATATYPE_FLOAT)

        self.updClient.sendFloat(rX)
        self.updClient.sendFloat(rY)
        self.updClient.sendFloat(rZ)
        self.updClient.sendByte(rA)

        self.updClient.sendFloat(aX)
        self.updClient.sendFloat(aY)
        self.updClient.sendFloat(aZ)
        self.updClient.sendByte(aA)

        self.updClient.sendFloat(mX)
        self.updClient.sendFloat(mY)
        self.updClient.sendFloat(mZ)
        self.updClient.sendByte(mA)

        self.updClient.endPacket()


    def sendInspectionFusedIMUData(self,sensorId,quaternion):
        if(not self.connected):
            return

        self.updClient.beginPacket()

        self.updClient.sendPacketType(PACKET_INSPECTION)
        self.updClient.sendPacketNumber()

        self.updClient.sendByte(PACKET_INSPECTION_PACKETTYPE_FUSED_IMU_DATA)

        self.updClient.sendByte(sensorId)
        self.updClient.sendByte(PACKET_INSPECTION_DATATYPE_FLOAT)

        self.updClient.sendFloat(quaternion.x)
        self.updClient.sendFloat(quaternion.y)
        self.updClient.sendFloat(quaternion.z)
        self.updClient.sendFloat(quaternion.w)

        self.updClient.endPacket()


    def sendInspectionCorrectionData(self,sensorId,quaternion):
        if(not self.connected):
            return

        self.updClient.beginPacket()
        self.updClient.sendPacketType(PACKET_INSPECTION)
        self.updClient.sendPacketNumber()
        self.updClient.sendByte(PACKET_INSPECTION_PACKETTYPE_CORRECTION_DATA)
        self.updClient.sendByte(sensorId)
        self.updClient.sendByte(PACKET_INSPECTION_DATATYPE_FLOAT)
        self.updClient.sendFloat(quaternion.x)
        self.updClient.sendFloat(quaternion.y)
        self.updClient.sendFloat(quaternion.z)
        self.updClient.sendFloat(quaternion.w)
        self.updClient.endPacket()

    def sendHandshake(self):
        self.updClient.beginPacket()
        self.updClient.sendPacketType(PACKET_HANDSHAKE)
        self.updClient.sendLong(0)
        self.updClient.sendInt(BOARD)
        self.updClient.sendInt(IMU)
        self.updClient.sendInt(HARDWARE_MCU)
        self.updClient.sendInt(3)
        self.updClient.sendInt(4)
        self.updClient.sendInt(5)
        self.updClient.sendInt(FIRMWARE_BUILD_NUMBER)
        self.updClient.sendShortString(FIRMWARE_VERSION)
        self.updClient.sendStringByte(format(uuid.getnode(), 'x'))
        self.updClient.endPacket()

    def returnLastPacket(self,incomingPacket):
        self.updClient.beginPacket()
        self.updClient.sendBytes(incomingPacket)
        self.updClient.endPacket()

    def connect(self):
        timeStamp = time.time()
        while True:
            if(len(self.updClient.msg_recv)>0):
                package = self.updClient.popMessage()
                if package[0] == PACKET_HANDSHAKE:
                    host , port = self.updClient.msg_recv_from
                    self.updClient.setHost(host)
                    self.updClient.setPort(port)
                    self.connected = True
                    self.logging("Connected")
            else:
                break
        if self.lastConnectionAttemptMs + 1 < timeStamp:
            self.lastConnectionAttemptMs = timeStamp
            self.logging("Looking for the server...")
            self.sendHandshake()

    def update(self,sensors):
        if self.connected:
            if(len(self.updClient.msg_recv)>0):
                package = self.updClient.popMessage()
                receive = int.from_bytes(package[:4], "big")
                if receive == PACKET_RECEIVE_HEARTBEAT:
                    self.logging("PACKET_RECEIVE_HEARTBEAT")
                    self.sendHeartbeat()
                elif receive == PACKET_RECEIVE_VIBRATE:
                    self.logging("PACKET_RECEIVE_VIBRATE")
                elif receive == PACKET_RECEIVE_HANDSHAKE:
                    self.logging("PACKET_RECEIVE_HANDSHAKE")
                elif receive == PACKET_RECEIVE_COMMAND:
                    self.logging("PACKET_RECEIVE_COMMAND")
                elif receive == PACKET_CONFIG:
                    self.logging("PACKET_CONFIG")
                elif receive == PACKET_PING_PONG:
                    self.logging("PACKET_PING_PONG")
                    self.returnLastPacket(package)
                elif receive == PACKET_SENSOR_INFO:
                    self.logging("PACKET_SENSOR_INFO")
                    if len(package) < 6:
                        self.logging("Wrong sensor info packet")
                    else:
                        self.sensorStateNotified[int(package[4])] = int(package[5])

            if self.updClient.lastPacketMs + TIMEOUT < time.time():
                self.connected = False
                self.sensorStateNotified = np.zeros(self.sensorCount, dtype=int)
                self.logging("Connection to server timed out")
        
        if not self.connected:
            self.connect()
        elif(not np.array_equal(sensors.sensorState,self.sensorStateNotified)):
            self.logging("updateSensorState")
            self.updateSensorState(sensors)
                
    def updateSensorState(self,sensors):
        if time.time() - self.lastSensorInfoPacket > 1:
            self.lastSensorInfoPacket = time.time()
            for i in range(0,self.sensorCount):
                if(sensors.sensorState[i] != self.sensorStateNotified[i]):
                    print(sensors.getSensor(i))
                    self.sendSensorInfo(sensors.getSensor(i))
                    self.sensorStateNotified[i] = sensors.sensorState[i]
            
