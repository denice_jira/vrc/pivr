import logging

class Logger(object):

    def __init__(self,module):
        self.module = module
        format = "%(asctime)s %(message)s"
        logging.basicConfig(format=format, level=logging.INFO,datefmt="%H:%M:%S")

    def logging(self,message):
        logging.info('[%s] : %s',self.module, message)