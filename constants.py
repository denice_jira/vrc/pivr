PACKET_HEARTBEAT = 0
PACKET_HANDSHAKE = 3
PACKET_ACCEL = 4
PACKET_RAW_CALIBRATION_DATA = 6
PACKET_CALIBRATION_FINISHED = 7
PACKET_CONFIG = 8
PACKET_PING_PONG = 10
PACKET_SERIAL = 11
PACKET_BATTERY_LEVEL = 12
PACKET_TAP = 13
PACKET_ERROR = 14
PACKET_SENSOR_INFO = 15
PACKET_ROTATION_DATA = 17
PACKET_MAGNETOMETER_ACCURACY = 18
PACKET_SIGNAL_STRENGTH = 19
PACKET_TEMPERATURE = 20

PACKET_INSPECTION = 105
PACKET_RECEIVE_HEARTBEAT = 1
PACKET_RECEIVE_VIBRATE = 2
PACKET_RECEIVE_HANDSHAKE = 3
PACKET_RECEIVE_COMMAND = 4

PACKET_INSPECTION_PACKETTYPE_RAW_IMU_DATA = 1
PACKET_INSPECTION_PACKETTYPE_FUSED_IMU_DATA = 2
PACKET_INSPECTION_PACKETTYPE_CORRECTION_DATA = 3
PACKET_INSPECTION_DATATYPE_INT = 1
PACKET_INSPECTION_DATATYPE_FLOAT = 2

FIRMWARE_VERSION = "1.2.3"
FIRMWARE_BUILD_NUMBER = 10
HARDWARE_MCU = 1
IMU = 2
BOARD = 3

TIMEOUT = 3

LOGGING_PACKET = 0