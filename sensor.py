from logger import Logger
import threading
import numpy as np
import random
class Sensor(Logger):
    def __init__(self,sensorCount,network):
        super().__init__("Sensor")
        self.sensorCount = sensorCount
        self.sensorState = np.ones(sensorCount, dtype=int)
        self.quaternion = []
        self.isSend = np.ones(sensorCount, dtype=int)
        self.network = network
        for i in range(0,sensorCount):
            self.quaternion.append(np.zeros(4))

        x = threading.Thread(target=self.thread_send_data, args=())
        x.start()

    def getSensor(self,i):
        return {
        "sensorId": i,
        "sensorState": 1,
        "sensorType": 0
        }
    def getQuaternion(self,i):
        return self.quaternion[i]
    
    def getQuaternions(self,i):
        return self.quaternion
    
    def setQuaternion(self,i,x,y,z,w):
        self.quaternion[i] = [x,y,z,w]
        self.isSend = 0
    
    def sendData(self):
        for i in range(0,self.sensorCount):
            if(self.isSend[i] == 0):
                quaternion = {
                    "x":self.quaternion[i][0],
                    "y":self.quaternion[i][1],
                    "z":self.quaternion[i][2],
                    "w":self.quaternion[i][3]
                }
                self.network.sendRotationData(quaternion, 1, 0, i)
    
    def mockDate(self,i):
        self.isSend[i] = 0
        self.quaternion[i][0] = round(random.uniform(-1,1), 2)
        self.quaternion[i][1] = round(random.uniform(-1,1), 2)
        self.quaternion[i][2] = round(random.uniform(-1,1), 2)
        self.quaternion[i][3] = round(random.uniform(-1,1), 2)

    def mockAll(self):
        for i in range(0,self.sensorCount):
            self.mockDate(i)

    def thread_send_data(self):
        self.logging("thread_send_data: starting")
        while True:
            self.sendData()