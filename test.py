from network import Network
import numpy as np
from sensor import Sensor

test = Network(8)
sensor = Sensor(8,test)
while True:
    test.update(sensor)
    sensor.mockAll()
    sensor.sendData()