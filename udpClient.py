import socket 
import struct
import time 
from logger import Logger
import threading
from constants import *

class UpdClient(Logger):
    def __init__(self):
        super().__init__("UpdClient")
        self.host = "255.255.255.255"
        self.port = 6969
        self.connection = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.connection.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        self.msg = None
        self.msg_recv = []
        self.msg_recv_from = None
        self.lastPacketMs = 0
        self.packetNumber = 0
        x = threading.Thread(target=self.thread_recvfrom, args=())
        x.start()

    def setHost(self,host):
        self.host = host

    def setPort(self,port):
        self.port = port

    def popMessage(self):
        return self.msg_recv.pop(0)

    def thread_recvfrom(self):
        self.logging("thread_recvfrom: starting")
        while True:
            if len(self.msg_recv) == 0:
                data, address = self.connection.recvfrom(4096)
                self.msg_recv.append(data)
                self.msg_recv_from = address
                self.lastPacketMs = time.time()

    def sendTo(self,message):
        self.connection.sendto(message, (self.host, self.port))

    def beginPacket(self):
        self.msg = None
    
    def endPacket(self):
        self.printMsg() #DEBUG
        self.sendTo(self.msg)

    def sendPacketNumber(self):
        self.sendLong(self.packetNumber)
        self.packetNumber = self.packetNumber + 1

    def sendPacketType(self,t):
        self.byteAppend(b'\x00\x00\x00')
        self.byteAppend((t).to_bytes(1, byteorder='big'))
        
    def sendFloat(self,f):
        self.byteAppend(bytearray(struct.pack("f",f)))

    def sendByte(self,b):
        if type(b) == "bytes":
            self.byteAppend(b)
        else:
            self.byteAppend((b).to_bytes(1, byteorder='big'))

    def sendInt(self,i):
        self.byteAppend((i).to_bytes(4, byteorder='big'))

    def sendLong(self,l):
        self.byteAppend((l).to_bytes(8, byteorder='big'))

    def sendBytes(self,b):
        self.byteAppend(b)

    def sendStringByte(self,s):
        self.byteAppend(bytes(str.encode(s)))

    def sendShortString(self,s):
        size = len(s)
        self.byteAppend((size).to_bytes(1, byteorder='big'))
        self.byteAppend(bytes(str.encode(s)))

    def sendLongString(self,s):
        size = len(s)
        self.sendInt(size)
        self.byteAppend(bytes(str.encode(s)))

    def byteAppend(self,msg):
        if self.msg is None:
            self.msg = msg
        else:
            self.msg = self.msg + msg
        
    def printMsg(self):
        if LOGGING_PACKET == 1:
            self.logging("sending " + str(self.msg))